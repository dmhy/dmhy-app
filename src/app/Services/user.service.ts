import { Injectable } from '@angular/core';
import { TokenInfo } from '../Models/TokenInfo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private http: HttpClient
  ) { }

  private Post<T>(address: string): Observable<T> {
    return this.http.post<T>(`${environment.authAddress}/${address}`, {
      withCredentials: true
    }).pipe(catchError(this.handleError));
  }

  public getToken(): Observable<TokenInfo> {
    return this.Post('api/Login');
  }

  private handleError(error: any): Promise<any> {
    console.error(error);
    return Promise.reject(error.message || error);
  }
}
