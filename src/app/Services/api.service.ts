import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from './base.service';
import { Topic } from '../Models/Topic';
import { Result } from '../Models/Result';
import { HotPost } from '../Models/hotPost';
import { Programme } from '../Models/Programme';
import { Category } from '../Models/Category';
import { PostDetail } from '../Models/PostDetail';
import { PostComment } from '../Models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private baseService: BaseService
  ) { BaseService.serverAddress = environment.serverAddress; }

  public getTopics(currentPage: number): Observable<Result<Topic[]>> {
    return this.baseService.Get(`/api/DmhyPost/Topics/${currentPage}`);
  }

  public hotPosts(): Observable<Result<HotPost[]>> {
    return this.baseService.Get(`/api/DmhyInfo/HotPosts`);
  }

  public programme(): Observable<Result<Programme[]>> {
    return this.baseService.Get(`/api/DmhyInfo/Programme`);
  }

  public categoies(): Observable<Result<Category[]>> {
    return this.baseService.Get(`/api/DmhyInfo/Categoies`);
  }

  public teams(): Observable<Result<Category[]>> {
    return this.baseService.Get(`/api/DmhyInfo/Teams`);
  }

  public getPosts(categoy: string, id: number, currentPage: number): Observable<Result<Topic[]>> {
    return this.baseService.Get(`/api/DmhyPost/${categoy}/${currentPage}?${categoy}Id=${id}`);
  }

  public searchPosts(keyword: string, currentPage: number): Observable<Result<Topic[]>> {
    return this.baseService.Get(`/api/DmhyPost/Search/${currentPage}?keyword=${keyword}`);
  }

  public postDetatil(htmlId: string): Observable<Result<PostDetail>> {
    return this.baseService.Get(`/api/DmhyPostDetailed/Content/${htmlId}`);
  }

  public postComments(htmlId: string): Observable<Result<PostComment[]>> {
    return this.baseService.Get(`/api/DmhyPostDetailed/Comments/${htmlId}`);
  }
}

