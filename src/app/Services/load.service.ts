import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadService {

  private subject = new Subject<any>();

  constructor() { this.subject.next(); }

  setLoadingStatus(status: boolean, title: string) {
    this.subject.next({ status: status, title: title });
  }

  getLoadingInfo(): Observable<any> {
    return this.subject.asObservable();
  }
}
