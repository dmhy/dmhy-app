import { Injectable } from '@angular/core';
import { CacheModel } from '../Models/CacheModel';
import { TokenInfo } from '../Models/TokenInfo';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  public static cachedData: CacheModel = new CacheModel();

  constructor() { }

  public GetTokenInfo(): TokenInfo {
    return CacheService.cachedData.tokeInfo;
  }

  public UpdateTokenInfo(tokeInfo: TokenInfo): void {
    CacheService.cachedData.tokeInfo = tokeInfo;
  }
}
