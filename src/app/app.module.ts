import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppRoutingModule } from './Modules/app-routing.module';

import { AppComponent } from './Controllers/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, zh_CN, NZ_MESSAGE_CONFIG } from 'ng-zorro-antd';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';
import { CategoryComponent } from './Controllers/category.component';
import { TeamsComponent } from './Controllers/teams.component';
import { ProgrammeComponent } from './Controllers/programme.component';
import { AboutComponent } from './Controllers/about.component';
import { HotspotComponent } from './Controllers/hotspot.component';
import { TopicsComponent } from './Controllers/topics.component';
import { UserComponent } from './Controllers/user.component';

import { UserService } from './Services/user.service';
import { JwtInterceptor } from './Helpers/jwt.interceptor';
import { ApiService } from './Services/api.service';
import { CacheService } from './Services/cache.service';
import { SafeUrPipe } from './Helpers/safe-url.pipe';
import { LoadService } from './Services/load.service';
import { PostsComponent } from './Controllers/posts.component';
import { SearchPostsComponent } from './Controllers/search-posts.component';
import { PostDetailComponent } from './Controllers/post-detail.component';
import { SafeHtmlPipe } from './Helpers/safe-html.pipe';
import { environment } from '../environments/environment';



registerLocaleData(zh);

@NgModule({
  declarations: [
    AppComponent,
    HotspotComponent,
    CategoryComponent,
    TeamsComponent,
    ProgrammeComponent,
    AboutComponent,
    TopicsComponent,
    UserComponent,
    SafeUrPipe,
    PostsComponent,
    SearchPostsComponent,
    PostDetailComponent,
    SafeHtmlPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    AppRoutingModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    UserService,
    ApiService,
    CacheService,
    LoadService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: NZ_I18N, useValue: zh_CN },
    { provide: NZ_MESSAGE_CONFIG, useValue: { nzDuration: 3000, nzMaxStack: 1 } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
