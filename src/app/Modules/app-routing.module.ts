import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from '../Controllers/app.component';
import { HotspotComponent } from '../Controllers/hotspot.component';
import { CategoryComponent } from '../Controllers/category.component';
import { TeamsComponent } from '../Controllers/teams.component';
import { ProgrammeComponent } from '../Controllers/programme.component';
import { AboutComponent } from '../Controllers/about.component';
import { TopicsComponent } from '../Controllers/topics.component';
import { UserComponent } from '../Controllers/user.component';
import { AuthGuard } from '../Helpers';
import { PostsComponent } from '../Controllers/posts.component';
import { SearchPostsComponent } from '../Controllers/search-posts.component';
import { PostDetailComponent } from '../Controllers/post-detail.component';


const routes: Routes = [
    { path: '', redirectTo: '/topics', pathMatch: 'full' },
    { path: 'topics', component: TopicsComponent, canActivate: [AuthGuard] },
    { path: 'posts/:category/:id', component: PostsComponent, canActivate: [AuthGuard] },
    { path: 'detail/:htmlId', component: PostDetailComponent, canActivate: [AuthGuard] },
    { path: 'hotspot', component: HotspotComponent, canActivate: [AuthGuard] },
    { path: 'categories', component: CategoryComponent, canActivate: [AuthGuard] },
    { path: 'teams', component: TeamsComponent, canActivate: [AuthGuard] },
    { path: 'programme', component: ProgrammeComponent, canActivate: [AuthGuard] },
    { path: 'search/:keyword', component: SearchPostsComponent, canActivate: [AuthGuard] },
    { path: 'about', component: AboutComponent },
    { path: 'user', component: UserComponent },
    { path: '**', redirectTo: '/topics' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
