import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CacheService } from '../Services/cache.service';

// auth guard用于防止未经身份验证的用户访问受限制的路由
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private cacheService: CacheService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.cacheService.GetTokenInfo()) {
      return true;
    }

    this.router.navigate(['/user'], { queryParams: { returnUrl: decodeURI(state.url) } });
    return false;
  }
}
