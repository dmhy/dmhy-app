import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CacheService } from '../Services/cache.service';
import { TokenInfo } from '../Models/TokenInfo';

// JWT拦截器拦截来自应用程序的http请求，以便在用户登录时将JWT身份验证令牌添加到Authorization标头。
@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(
        private cacheService: CacheService
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const info: TokenInfo = this.cacheService.GetTokenInfo();
        if (info && info.access_token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `${info.token_type} ${info.access_token}`
                }
            });
        }

        return next.handle(request);
    }
}
