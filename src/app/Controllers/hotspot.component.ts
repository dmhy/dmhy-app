import { Component, OnInit } from '@angular/core';
import { ApiService } from '../Services/api.service';
import { HotPost } from '../Models/hotPost';
import { NzMessageService } from 'ng-zorro-antd';
import { LoadService } from '../Services/load.service';

@Component({
  selector: 'app-hotspot',
  templateUrl: '../Views/hotspot.component.html',
  styleUrls: ['../Css/hotspot.component.css']
})
export class HotspotComponent implements OnInit {

  hotPosts: HotPost[];

  constructor(
    private apiService: ApiService,
    private message: NzMessageService,
    private loadService: LoadService
  ) { }

  ngOnInit() {
    this.loadService.setLoadingStatus(true, '加载中...');

    this.apiService.hotPosts()
      .subscribe(
        result => {

          this.loadService.setLoadingStatus(false, '');
          if (result.status !== 1) {
            this.message.warning(result.errorMsg);
            return;
          }

          this.hotPosts = result.data;
        },
        error => {
          this.loadService.setLoadingStatus(false, '');
          this.message.error(`获取资源失败！请稍后重试。${error}`);
        });
  }

}
