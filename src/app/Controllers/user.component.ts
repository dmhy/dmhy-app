import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../Services/user.service';
import { CacheService } from '../Services/cache.service';
import { NzMessageService } from 'ng-zorro-antd';
import { LoadService } from '../Services/load.service';


@Component({
  selector: 'app-user',
  templateUrl: '../Views/user.component.html',
  styleUrls: ['../Css/user.component.css']
})
export class UserComponent implements OnInit {

  returnUrl: string;

  constructor(
    private userService: UserService,
    private cacheService: CacheService,
    private route: ActivatedRoute,
    private router: Router,
    private message: NzMessageService,
    private loadService: LoadService
  ) { }

  ngOnInit() {

    this.loadService.setLoadingStatus(true, '正在连接...');

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    if (!this.cacheService.GetTokenInfo()) {
      this.userService.getToken()
        .subscribe(
          info => {
            this.loadService.setLoadingStatus(false, '');
            this.cacheService.UpdateTokenInfo(info);
            this.router.navigate([this.returnUrl]); // 跳转
          },
          error => {
            this.loadService.setLoadingStatus(false, '');
            this.message.error(`连接失败：未能连接上服务器！${error}`);
          });
    }
  }

}
