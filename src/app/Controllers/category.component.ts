import { Component, OnInit } from '@angular/core';
import { ApiService } from '../Services/api.service';
import { Category } from '../Models/Category';
import { NzMessageService } from 'ng-zorro-antd';
import { LoadService } from '../Services/load.service';

@Component({
  selector: 'app-category',
  templateUrl: '../Views/category.component.html',
  styleUrls: ['../Css/category.component.css']
})
export class CategoryComponent implements OnInit {

  categories: Category[];

  constructor(
    private apiService: ApiService,
    private message: NzMessageService,
    private loadService: LoadService
  ) { }


  ngOnInit() {
    this.loadService.setLoadingStatus(true, '加载中...');

    this.apiService.categoies()
      .subscribe(
        result => {
          this.loadService.setLoadingStatus(false, '');
          if (result.status !== 1) {
            this.message.warning(result.errorMsg);
            return;
          }

          this.categories = result.data;
        },
        error => {
          this.loadService.setLoadingStatus(false, '');
          this.message.error('获取资源失败！请稍后重试。');
        });
  }

}
