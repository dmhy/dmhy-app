import { Component, OnInit } from '@angular/core';
import { ApiService } from '../Services/api.service';
import { Topic } from '../Models/Topic';
import { LoadService } from '../Services/load.service';
import { NzMessageService } from 'ng-zorro-antd';
import { Result } from '../Models/Result';
@Component({
  selector: 'app-topics',
  templateUrl: '../Views/topics.component.html',
  styleUrls: ['../Css/topics.component.css']
})
export class TopicsComponent implements OnInit {

  currentPage = 1;
  topics: Topic[];
  isLoading = false;

  constructor(
    private apiService: ApiService,
    private message: NzMessageService,
    private loadService: LoadService
  ) { }

  ngOnInit() {
    this.loadPost(1);
  }

  loadMorePost() {
    this.currentPage++;
    this.isLoading = true;
    this.loadPost(this.currentPage);

  }

  private loadPost(currentPage: number) {
    this.loadService.setLoadingStatus(true, '加载中...');
    this.apiService.getTopics(currentPage)
      .subscribe(
        result => {
          this.resultFilter(result);
        },
        error => {
          this.loadService.setLoadingStatus(false, '');
          this.isLoading = false;
          this.message.error(`获取资源失败！请稍后重试。${error}`);
        });
  }

  private resultFilter(result: Result<Topic[]>) {

    this.loadService.setLoadingStatus(false, '');
    this.isLoading = false;
    if (result.status !== 1) {
      this.message.warning(result.errorMsg);
      return;
    }

    if (!result.data) {
      this.message.warning('没有更多资源！');
      return;
    }

    if (!this.topics) {
      this.topics = result.data;
      return;
    }

    this.topics = this.topics.concat(result.data);
  }

}
