import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ApiService } from '../Services/api.service';
import { Topic } from '../Models/Topic';
import { LoadService } from '../Services/load.service';
import { NzMessageService } from 'ng-zorro-antd';
import { ActivatedRoute, Router } from '@angular/router';
import { Result } from '../Models/Result';
@Component({
  selector: 'app-post',
  templateUrl: '../Views/topics.component.html'
})
export class PostsComponent implements OnInit {

  currentPage = 1;
  topics: Topic[];
  isLoading = false;
  category: string;
  id: number;

  constructor(
    private apiService: ApiService,
    private message: NzMessageService,
    private loadService: LoadService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {



    this.route.params.subscribe(
      params => {

        this.category = this.route.snapshot.paramMap.get('category');
        this.id = +this.route.snapshot.paramMap.get('id');

        if (!this.category || !this.id) {
          this.router.navigate(['/']);
          return;
        }
        this.topics = null;
        this.loadPost(this.category, this.id, 1);
      });

  }


  loadMorePost() {
    this.currentPage++;
    this.isLoading = true;
    this.loadPost(this.category, this.id, this.currentPage);
  }

  private loadPost(categoy: string, id: number, currentPage: number) {
    this.loadService.setLoadingStatus(true, '加载中...');
    this.apiService.getPosts(categoy, id, currentPage)
      .subscribe(
        result => {
          this.resultFilter(result);
        },
        error => {
          this.loadService.setLoadingStatus(false, '');
          this.isLoading = false;
          this.message.error('获取资源失败！请稍后重试。');
        });
  }

  private resultFilter(result: Result<Topic[]>) {
    this.loadService.setLoadingStatus(false, '');
    this.isLoading = false;
    if (result.status !== 1) {
      this.message.warning(result.errorMsg);
      return;
    }

    if (!result.data) {
      this.message.warning('没有更多资源！');
      return;
    }

    if (!this.topics) {
      this.topics = result.data;
      return;
    }

    this.topics = this.topics.concat(result.data);

  }
}
