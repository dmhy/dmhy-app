import { Component, OnInit } from '@angular/core';
import { Team } from '../Models/Team';
import { ApiService } from '../Services/api.service';
import { NzMessageService } from 'ng-zorro-antd';
import { LoadService } from '../Services/load.service';

@Component({
  selector: 'app-teams',
  templateUrl: '../Views/teams.component.html',
  styleUrls: ['../Css/teams.component.css']
})
export class TeamsComponent implements OnInit {

  teams: Team[];

  constructor(
    private apiService: ApiService,
    private message: NzMessageService,
    private loadService: LoadService
  ) { }

  ngOnInit() {
    this.loadService.setLoadingStatus(true, '加载中...');

    this.apiService.teams()
      .subscribe(
        result => {
          this.loadService.setLoadingStatus(false, '');
          if (result.status !== 1) {
            this.message.warning(result.errorMsg);
            return;
          }

          this.teams = result.data;
        },
        error => {
          this.loadService.setLoadingStatus(false, '');
          this.message.error(`获取资源失败！请稍后重试。${error}`);
        });
  }

}
