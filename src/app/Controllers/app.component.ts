import { Component, OnInit, OnDestroy, TemplateRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Subscription } from 'rxjs';
import { LoadService } from '../Services/load.service';
import { Router, NavigationEnd } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: '../Views/app.component.html',
  styleUrls: ['../Css/app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  isLoading = false;
  loadingTitle = '';
  isCollapsed = true;
  triggerTemplate = null;
  clientWidth = document.documentElement.clientWidth;
  keyword: string;
  isShowSearchBox: false;
  private subscription: Subscription;

  public constructor(
    private location: PlatformLocation,
    private loadService: LoadService,
    private message: NzMessageService,
    private ref: ChangeDetectorRef,
    private router: Router
  ) { }

  routes = {
    'topics': false,
    'hotspot': false,
    'categories': false,
    'teams': false,
    'programme': false,
    'about': false,
  };

  ngOnInit() {

    // 添加订阅者
    this.subscription = this.loadService.getLoadingInfo()
      .subscribe(info => {
        if (info) {
          this.isLoading = info.status;
          this.loadingTitle = info.title;
          // 更新视图
          this.ref.detectChanges();
        }

      });


    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(x => {
      window.scrollTo(0, 0);
    });

    this.setSelectedMenuItem();
  }


  setSelectedMenuItem(): void {
    const routeName = this.location.pathname.replace('/', '');

    if (!routeName) {
      this.routes['topics'] = true;
      return;
    }

    this.routes[routeName] = true;
  }

  toggleCollapsed(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  collapsedNav(): void {
    const width = document.documentElement.clientWidth;

    if (width <= 768) {
      this.isCollapsed = true;
    }

  }

  contentcollapsedNav() {
    const width = document.documentElement.clientWidth;

    if (width <= 768) {
      this.isShowSearchBox = false;
      this.isCollapsed = true;
    }

  }

  searchPost() {

    if (!this.keyword) {
      this.message.warning('请输入番剧名称');
      return;
    }

    const width = document.documentElement.clientWidth;
    if (width <= 768) {
      this.isCollapsed = true;
    }
    this.router.navigate([`/search/${this.keyword}`]);
    this.keyword = '';
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
