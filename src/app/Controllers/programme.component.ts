import { Component, OnInit } from '@angular/core';
import { ApiService } from '../Services/api.service';
import { Programme } from '../Models/Programme';
import { Dramas } from '../Models/Dramas';
import { NzMessageService } from 'ng-zorro-antd';
import { LoadService } from '../Services/load.service';

@Component({
  selector: 'app-programme',
  templateUrl: '../Views/programme.component.html',
  styleUrls: ['../Css/programme.component.css']
})
export class ProgrammeComponent implements OnInit {
  tabs = [
    { index: 0, name: '星期天' },
    { index: 1, name: '星期一' },
    { index: 2, name: '星期二' },
    { index: 3, name: '星期三' },
    { index: 4, name: '星期四' },
    { index: 5, name: '星期五' },
    { index: 6, name: '星期六' }
  ];
  nzTabPosition = 'top';
  selectedIndex: number;
  currentDay = new Date().getDay();
  programmes: Programme[];
  currentDramas: Dramas[];

  constructor(
    private apiService: ApiService,
    private message: NzMessageService,
    private loadService: LoadService
  ) { }

  ngOnInit(): void {

    this.loadService.setLoadingStatus(true, '加载中...');

    this.apiService.programme().subscribe(
      result => {

        this.loadService.setLoadingStatus(false, '');
        if (result.status !== 1) {
          this.message.warning(result.errorMsg);
          return;
        }

        this.programmes = result.data;
        this.selectedIndex = this.currentDay;
        this.currentDramas = this.programmes[this.selectedIndex].dramas;
      },
      error => {
        this.loadService.setLoadingStatus(false, '');
        this.message.error(`获取资源失败！请稍后重试。${error}`);
      });

  }

  tabsNext(): void {
    this.selectedIndex++;
    this.currentDramas = this.programmes[this.selectedIndex].dramas;
  }

  tabsPrev(): void {
    this.selectedIndex--;
    this.currentDramas = this.programmes[this.selectedIndex].dramas;
  }

  tabsChange(index): void {
    this.currentDramas = this.programmes[index].dramas;
  }

}
