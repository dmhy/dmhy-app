import { Component, OnInit } from '@angular/core';
import { ApiService } from '../Services/api.service';
import { NzMessageService } from 'ng-zorro-antd';
import { LoadService } from '../Services/load.service';
import { ActivatedRoute } from '@angular/router';
import { PostDetail, PostComment, Result } from '../Models';

@Component({
  selector: 'app-post-detatil',
  templateUrl: '../Views/post-detatil.component.html',
  styleUrls: ['../Css/post-detatil.component.css']
})
export class PostDetailComponent implements OnInit {

  htmlId: string;
  detatil: PostDetail;
  commments: PostComment[];
  isLoad = false;

  constructor(
    private apiService: ApiService,
    private message: NzMessageService,
    private loadService: LoadService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.route.params.subscribe(
      params => {
        this.htmlId = decodeURI(this.route.snapshot.paramMap.get('htmlId'));
        this.postDetatil(this.htmlId);
      });
  }

  private postDetatil(htmlId: string): void {
    this.loadService.setLoadingStatus(true, '加载中...');
    this.apiService.postDetatil(htmlId)
      .subscribe(
        result => {

          if (!this.resultFilter(result)) {
            return;
          }

          // 限制图片宽度
          result.data.content = result.data.content
            .replace(/<img/g, '<img style="width:100%" ')
            .replace(/<input/g, '<input style="width:100%" ');

          // 替换默认图片
          result.data.user.userAvatarImg = result.data.user.userAvatarImg
            .replace('/images/defaultUser.png', '/assets/images/defaultUser.png');

          if (result.data.team.teamName) {
            result.data.team.teamLogoUrl = result.data.team.teamLogoUrl
              .replace('/images/defaultTeam.gif', '/assets/images/defaultTeam.gif');
          }

          this.detatil = result.data;
        },
        error => {
          this.loadService.setLoadingStatus(false, '');
          this.message.error(`获取资源失败！请稍后重试。${error}`);
        });
  }

  private loadComments(): void {
    this.isLoad = true;
    this.loadService.setLoadingStatus(true, '加载中...');

    this.apiService.postComments(this.htmlId)
      .subscribe(
        result => {
          this.isLoad = false;
          if (!this.resultFilter(result)) {
            return;
          }
          result.data.forEach(element => {
            element.userInfo.profilePictureUrl = element.userInfo.profilePictureUrl
              .replace('/images/defaultUser.png', '/assets/images/defaultUser.png');
          });

          this.commments = result.data;
        },
        error => {
          this.loadService.setLoadingStatus(false, '');
          this.message.error('获取内容失败！请稍后重试。');
        });
  }

  private resultFilter<T>(result: Result<T>): boolean {

    this.loadService.setLoadingStatus(false, '');

    if (result.status !== 1) {
      this.message.warning(result.errorMsg);
      return false;
    }

    if (!result.data) {
      this.message.warning('没有获取到帖子内容！');
      return false;
    }

    return true;
  }
}
