import { Component, OnInit } from '@angular/core';
import { ApiService } from '../Services/api.service';
import { Topic } from '../Models/Topic';
import { LoadService } from '../Services/load.service';
import { NzMessageService } from 'ng-zorro-antd';
import { ActivatedRoute, Router } from '@angular/router';
import { Result } from '../Models/Result';

@Component({
  selector: 'app-search-posts',
  templateUrl: '../Views/topics.component.html'
})
export class SearchPostsComponent implements OnInit {

  currentPage = 1;
  topics: Topic[];
  isLoading = false;
  keyword: string;

  constructor(
    private apiService: ApiService,
    private message: NzMessageService,
    private loadService: LoadService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  searchPost(keyword: string, currentPage: number) {
    this.loadService.setLoadingStatus(true, '加载中...');
    this.apiService.searchPosts(keyword, currentPage)
      .subscribe(
        result => {
          this.resultFilter(result);
        },
        error => {
          this.loadService.setLoadingStatus(false, '');
          this.isLoading = false;
          this.message.error('获取资源失败！请稍后重试。');
        });
  }

  loadMorePost() {
    this.currentPage++;
    this.isLoading = true;
    this.searchPost(this.keyword, this.currentPage);
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.keyword = decodeURI(this.route.snapshot.paramMap.get('keyword'));
        this.topics = null;
        this.searchPost(this.keyword, 1);
      });

  }


  private resultFilter(result: Result<Topic[]>) {

    this.loadService.setLoadingStatus(false, '');
    this.isLoading = false;

    if (result.status !== 1 && this.currentPage === 1) {
      this.message.warning(result.errorMsg);
      this.router.navigate(['/']);
    }

    if (result.status !== 1) {
      this.message.warning(result.errorMsg);
      return;
    }

    if (!result.data) {
      this.message.warning('没有更多资源！');
      return;
    }

    if (!this.topics) {
      this.topics = result.data;
      return;
    }

    this.topics = this.topics.concat(result.data);

  }

}
