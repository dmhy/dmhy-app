export class PostComment {
    public id: number;
    public userInfo: CommentUserInfo;
    public content: string;
}

class CommentUserInfo {
    public name: string;
    public dateTime: string;
    public iPAddress: string;
    public profilePictureUrl: string;
}
