import { Dramas } from './Dramas';

export class Programme {
    public dramas: Dramas[];
}
