export class Result<T> {
    public count: number;
    public status: number;
    public title: string;
    public data: T;
    public errorMsg: string;
}
