import { Category } from './Category';
import { Team } from './Team';

export class Topic {
    public id: string;
    public dateTime: string;
    public category: Category;
    public team: Team;
    public title: string;
    public htmlId: string;
    public downloadArrow: string;
    public fileSize: string;
    public userName: string;
    public userId: number;
}
