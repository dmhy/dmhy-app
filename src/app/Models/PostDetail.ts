import { Topic } from './Topic';

export class PostDetail {
    public post: Topic;
    public user: DPostDetailedUser;
    public team: DPostDetailedTeam;
    public content: string;
    public btDict: any[];
    public btContentDict: any[];
}

class DPostDetailedUser {
    public userAvatarImg: string;
    public userName: string;
    public userId: string;
}

class DPostDetailedTeam {
    public teamLogoUrl: string;
    public teamName: string;
    public teamId: string;
}
